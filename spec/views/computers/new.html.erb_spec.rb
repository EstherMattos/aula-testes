require 'rails_helper'

RSpec.describe "computers/new", type: :view do
  before(:each) do
    assign(:computer, Computer.new(
      :processor => "MyString",
      :gpu => "MyString",
      :ram => "",
      :psu => 1
    ))
  end

  it "renders new computer form" do
    render

    assert_select "form[action=?][method=?]", computers_path, "post" do

      assert_select "input[name=?]", "computer[processor]"

      assert_select "input[name=?]", "computer[gpu]"

      assert_select "input[name=?]", "computer[ram]"

      assert_select "input[name=?]", "computer[psu]"
    end
  end
end
