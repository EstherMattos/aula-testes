require 'rails_helper'

RSpec.describe Computer, type: :model do
  it "cannot have less than 8GB of ram" do
    teste = Computer.new(processor:"Ryzen 7 3900", gpu: "Radeon VII", ram: 7, storage:1025, psu: 750)
    expect(teste.save).to eq false
  end
  it "cannot have less than 1TB(1024GB) of storage" do
    teste = Computer.new(processor:"Ryzen 7 3900", gpu: "Radeon VII", ram: 8, storage:1020, psu: 750)
    expect(teste.save).to eq false
  end
  it "ram must be a power of two" do
    teste = Computer.new(processor:"Ryzen 7 3900", gpu: "Radeon VII", ram: 9, storage:1024, psu: 750)
    expect(teste.save).to eq false
  end

end

