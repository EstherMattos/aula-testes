require 'rails_helper'

RSpec.describe "computers/edit", type: :view do
  before(:each) do
    @computer = assign(:computer, Computer.create!(
      :processor => "MyString",
      :gpu => "MyString",
      :ram => "",
      :psu => 1
    ))
  end

  it "renders the edit computer form" do
    render

    assert_select "form[action=?][method=?]", computer_path(@computer), "post" do

      assert_select "input[name=?]", "computer[processor]"

      assert_select "input[name=?]", "computer[gpu]"

      assert_select "input[name=?]", "computer[ram]"

      assert_select "input[name=?]", "computer[psu]"
    end
  end
end
