require 'rails_helper'

RSpec.describe "computers/show", type: :view do
  before(:each) do
    @computer = assign(:computer, Computer.create!(
      :processor => "Processor",
      :gpu => "Gpu",
      :ram => "",
      :psu => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Processor/)
    expect(rendered).to match(/Gpu/)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
  end
end
