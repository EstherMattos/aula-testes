require 'rails_helper'

RSpec.describe "computers/index", type: :view do
  before(:each) do
    assign(:computers, [
      Computer.create!(
        :processor => "Processor",
        :gpu => "Gpu",
        :ram => "",
        :psu => 2
      ),
      Computer.create!(
        :processor => "Processor",
        :gpu => "Gpu",
        :ram => "",
        :psu => 2
      )
    ])
  end

  it "renders a list of computers" do
    render
    assert_select "tr>td", :text => "Processor".to_s, :count => 2
    assert_select "tr>td", :text => "Gpu".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
