class CreateComputers < ActiveRecord::Migration[6.0]
  def change
    create_table :computers do |t|
      t.string :processor
      t.string :gpu
      t.float :ram
      t.integer :psu

      t.timestamps
    end
  end
end
