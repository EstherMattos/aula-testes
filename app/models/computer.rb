class Computer < ApplicationRecord
    validates :storage, :numericality => {greater_than_or_equal_to: 1024}
    validates :ram, :numericality => {greater_than_or_equal_to: 8}
    validate :is_a_power_of_two?

    private

    def is_a_power_of_two?
        ram = (self.ram.to_i).to_s 2
        ram.length.times do |x|
            if x>0
                if ram[x]=="1"
                    errors.add(:ram, "Must be a power of two")
                    break
                end
            end
        end
    end

end
