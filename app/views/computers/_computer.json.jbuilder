json.extract! computer, :id, :processor, :gpu, :ram, :psu, :created_at, :updated_at
json.url computer_url(computer, format: :json)
