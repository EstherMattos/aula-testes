class AddStorageToComputer < ActiveRecord::Migration[6.0]
  def change
    add_column :computers, :storage, :float
  end
end
